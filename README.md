# K-Map Calculator

- A website written in pure HTML, CSS, and JS, with 0 dependencies.
- Allows the user to input their Karnaugh map, with the website showing the simplified expression
- Allows the user to hover over parts of the expression to show the loops on the grid
- Supports 2, 3, and 4 input KMaps.
- Uses a custom rendition of the [espresso algorithm](https://en.wikipedia.org/wiki/Espresso_heuristic_logic_minimizer)

## [try it out!!](https://fazzi.gitlab.io/kmap-calculator)
