// Initializing 4 possible maximum variables in our K-Map
const varNames = new Array("A", "B", "C", "D");
// Width of KMap in 2, 3 and 4 inputs respectively
const KWid = new Array(2, 4, 4);
// Height of KMap in 2, 3 and 4 inputs respectively
const KHei = new Array(2, 2, 4);
// Number of variables in X direction in 2, 3 and 4 inputs respectively
const KVarX = new Array(1, 2, 2);
// Number of variables in Y direction in 2, 3 and 4 inputs respectively
const KVarY = new Array(1, 1, 2);
// bitOrd sets the order of the bits to be 00, 01, 11, 10
// 0, 1, 3, 2 are their denary representations
const bitOrd = new Array(0, 1, 3, 2);
// Background colour
const normalColor = "#1a1b2c";
// Hovering colour and colour for simplified output
const selectColor = "#5d79d1";
// Text color of the inputs
const textColor = "#c0caf5";
// Initialise the K-Map as 4 input
let numVar = 4;
// Initialize K-Map and loops as empty
let KMap = [];
let coverList = [];
let nCubeList = [];

function toBinString(val, b) {
  let str = val.toString(2);
  str = str.padStart(b, "0");
  return b === 0 ? "" : str;
}

const boolToBin = bool => (bool === 1 ? "1" : "0");

function initKMap(nVar, defaultValue) {
  KMap = [];
  KMap.Width = KWid[nVar - 2];
  KMap.Height = KHei[nVar - 2];
  KMap.nVarX = KVarX[nVar - 2];
  KMap.nVarY = KVarY[nVar - 2];

  for (let w = 0; w < KMap.Width; w++) {
    KMap[w] = [];
    for (let h = 0; h < KMap.Height; h++) {
      KMap[w][h] = {};
      // if defaultValue is 2, then randomize the value
      KMap[w][h].Value = defaultValue === 2 ? Math.round(Math.random()) : defaultValue;
      const valueStr = toBinString(bitOrd[w], KMap.nVarX) + toBinString(bitOrd[h], KMap.nVarY);

      KMap[w][h].Button_id = "KM" + valueStr;
      KMap[w][h].TD_id = "TD" + valueStr;
    }
  }
}

function resetKMap(fillGrid) {
  initKMap(numVar, fillGrid);
  redraw();
}

function changeNumVar(Num) {
  if (Num !== numVar) {
    numVar = Num;
    initKMap(Num, 0);
    document.getElementById("Var2").checked = Num === 2 ? true : false;
    document.getElementById("Var3").checked = Num === 3 ? true : false;
    document.getElementById("Var4").checked = Num === 4 ? true : false;
  }
  redraw();
}

function modifyKMEntry(entry) {
  // Flips the value of the entry
  if (entry.Value === 0) entry.Value = 1;
  else entry.Value = 0;
  redraw();
}

function setAllToNormalColor() {
  // For all items in array
  for (let w = 0; w < KMap.Width; w++) {
    for (let h = 0; h < KMap.Height; h++) {
      // Set background colour
      document.getElementById(KMap[w][h].Button_id).style.backgroundColor = normalColor;
      // Set textColor
      document.getElementById(KMap[w][h].Button_id).style.color = textColor;
    }
  }
}

// Set the color of entire loop
function setColor(nCube, color) {
  for (let i = 0; i < nCube.length; i++) {
    document.getElementById(
      KMap[nCube[i][0]][nCube[i][1]].Button_id,
    ).style.backgroundColor = color;
  }
}

// Checks whether a given n-cube can be accepted by the K-Map.
// An n-cube is accepted only if it contains no 0 values, and it contains at least one 1 value.
function checkCube(coords, sizes) {
  let no0val = true; // Remains true until a 0 value is found (we stop searching then).
  let has1val = false; // Remains false until a 1 value is found.
  for (let w = coords[0]; w < sizes[0] + coords[0] && no0val; w++) {
    for (let h = coords[1]; h < sizes[1] + coords[1] && no0val; h++) {
      no0val = no0val && KMap[w % KMap.Width][h % KMap.Height].Value;
      has1val = has1val || KMap[w % KMap.Width][h % KMap.Height].Value === 1;
    }
  }
  return no0val && has1val;
}

// Checks whether a given n-cube can be accepted by the K-Map.
// An n-cube is accepted only if it contains no 0 values, and it contains at least one 1 value.
function makeCube(coords, sizes) {
  const newCube = [];
  for (let w = coords[0]; w < sizes[0] + coords[0]; w++) {
    for (let h = coords[1]; h < sizes[1] + coords[1]; h++) {
      newCube.push([w % KMap.Width, h % KMap.Height]);
    }
  }
  return newCube;
}

// Within an n-cube array, eliminates any n-cubes whose spaces are entirely covered by other n-cubes in the array.
// aka. if an old loop is completely covered by a new, loop, we can eliminate the old one.
function checkForCollisions(nCubeArray) {
  const newCubeArray = [];
  const toKeep = [];
  let contained = false;
  for (let i = 0; i < nCubeArray.length; i++) {
    contained = false;
    for (let j = 0; j < nCubeArray.length; j++) {
      if (i !== j && nCubeArray[i].length < nCubeArray[j].length) {
        contained = contained || isContainedIn(nCubeArray[i], nCubeArray[j]);
      } else if (j < i && nCubeArray[i].length === nCubeArray[j].length) {
        contained = contained || isContainedIn(nCubeArray[i], nCubeArray[j]);
      }
    }
    if (!contained) {
      toKeep.push(i);
    }
  }
  for (let k = 0; k < toKeep.length; k++) {
    newCubeArray.push(nCubeArray[toKeep[k]]);
  }
  return newCubeArray;
}

// Checks if an n-cube is contained in another. Returns true if nCube1 is contained in nCube2.
// aka. return True if loop1 is completely within loop2
function isContainedIn(nCube1, nCube2) {
  let check = 0;
  let found = false;
  for (let i = 0; i < nCube1.length; i++) {
    found = false;
    for (let j = 0; j < nCube2.length; j++) {
      if (
        nCube1[i][0] === nCube2[j][0] &&
        nCube1[i][1] === nCube2[j][1] &&
        nCube1[i][2] === nCube2[j][2]
      ) {
        found = true;
      }
    }
    if (found) {
      check += 1;
    }
  }
  return check === nCube1.length;
}

// Returns true if a given space can already be found in a given list of (covered) spaces.
// Used to test whether a space that is detected as covered has been already added to the "covered spaces" list or not.
function alreadyCovered(space, cover) {
  let covered = false;
  for (let i = 0; i < cover.length; i++) {
    if (
      space[0] === cover[i][0] &&
      space[1] === cover[i][1] &&
      space[2] === cover[i][2]
    )
      covered = true;
  }
  return covered;
}

// Generates a list of covered spaces in the K-Map from a list of n-cubes (representing a cover of said map).
// aka a list of tiles covered by loops.

function getCoverList(nCubeArray) {
  const cover = [];

  for (const nCubeSet of nCubeArray) {
    for (const nCube of nCubeSet) {
      if (!alreadyCovered(nCube, cover)) {
        cover.push(nCube);
      }
    }
  }

  return cover;
}


/**
 * The "expand" step of the pseudo-ESPRESSO algorithm.
 *
 * Each new n-cube is expanded towards every direction (width, height, and square) until it cannot be expanded anymore,
 * creating various candidate "expanded" n-cubes. These cubes are then added to the n-cube list.
 * Finally, if any expanded n-cubes is contained within another expanded cube, it is removed from the list.
 */
function expand() {
  let newCubeSet = [];
  let w, h;

  // Function to add a cube to the set if it is valid
  function addCubeIfValid(position, dimensions, cubeSet) {
    if (checkCube(position, dimensions)) {
      cubeSet.push(makeCube(position, dimensions));
    }
  }

  // Loop through height
  for (h = 0; h < KMap.Height; h++) {
    // Loop through width
    for (w = 0; w < KMap.Width; w++) {
      newCubeSet = [];

      // Add 1x1s
      addCubeIfValid([w, h], [1, 1], newCubeSet);

      // Add 2x1s
      addCubeIfValid([w, h], [2, 1], newCubeSet);

      // Add 1x2s
      addCubeIfValid([w, h], [1, 2], newCubeSet);

      // Add 2x2s
      addCubeIfValid([w, h], [2, 2], newCubeSet);

      // These are only possible in 3 or 4 input Kmaps
      if (numVar >= 3) {
        // Add 4x1s
        addCubeIfValid([w, h], [4, 1], newCubeSet);
        // Add 4x2s
        addCubeIfValid([w, h], [4, 2], newCubeSet);
      }

      // These are only possible on 4 input Kmap
      if (numVar === 4) {
        // Add 1x4s
        addCubeIfValid([w, h], [1, 4], newCubeSet);
        // Add 2x4s
        addCubeIfValid([w, h], [2, 4], newCubeSet);
        // If entire kmap is covered, add 4x4
        addCubeIfValid([w, h], [4, 4], newCubeSet);
      }

      // Concatenate new cubes and check for collisions
      nCubeList = nCubeList.concat(checkForCollisions(newCubeSet));
    }
  }

  // Check for collisions at the end
  nCubeList = checkForCollisions(nCubeList);
}

/*
 * This function iterates over the list of n-cubes until no more cubes can be removed.
 *
 * If the covers are not identical (and if at least 1 1 is left out), the n-cube is considered essential
 * and is retained in the list.
 */
function irredundantCover() {
  coverList = getCoverList(nCubeList);
  let lastIter = false;
  let newNCubeList = [];
  let newCover = [];
  while (lastIter === false) {
    // If the previous iteration didn't remove any n-cubes, it becomes the last iteration and the algorithm stops.
    lastIter = true;
    for (let i = 0; i < nCubeList.length; i++) {
      // We iterate from the first n-cube on the list onwards. That is, from the top-left corner.
      newNCubeList = JSON.parse(JSON.stringify(nCubeList));
      newNCubeList.splice(i, 1);
      newCover = getCoverList(newNCubeList);
      // We check if the old cover is contained in the new one, to see if they are the same (the new one is always contained in the old one).
      if (isContainedIn(coverList, newCover)) {
        nCubeList = newNCubeList;
        coverList = newCover;
        lastIter = false;
      }
    }
  }
}

// Reset nCubes and covers, and attempt to solve
function solve() {
  nCubeList = [];
  coverList = [];
  expand();
  // irredundantCover();
}

function generateKMapHTML() {
  let text = `<div style='text-align: center;'>`; // Centering
  let h, w; // 2d array;

  text += `<table style='margin-left: auto; margin-right: auto;'>`; // Centering

  // Line up the label e.g. AB with the middle
  text += `<tr><th></th><th></th><th></th><th colspan=${
    KMap.Width * KMap.Height + 2
  }>`;

  for (let i = 0; i < KMap.nVarX; i++) {
    text += varNames[i];
  }

  // Aligning Column and Row Labels
  text += `</th></tr>`;
  text += `<tr>`;
  text += `<th></th><th></th><th></th>`;

  // Columns
  for (w = 0; w < KMap.Width; w++) {
    text += `<th>${toBinString(bitOrd[w], KMap.nVarX)}</th>`;
  }
  text += `</tr>`;

  // Rows
  for (h = 0; h < KMap.Height; h++) {
    text += `<tr>`;
    if (h === 0) {
      text += `<th rowspan=${KMap.Height} style='vertical-align: middle;'>`;
      for (let j = 0; j < KMap.nVarY; j++) {
        text += varNames[j + KMap.nVarX];
      }
      text += `<th rowspan=${KMap.Height}>`;
    }
    text += `<th>${toBinString(bitOrd[h], KMap.nVarY)}</th>`;

    // Filling the matrix with buttons
    for (w = 0; w < KMap.Width; w++) {
      if (w === 0 && h === 1) text;
      text += `<td ID='${KMap[w][h].TD_id}';>`;
      text += `<input ID=${KMap[w][h].Button_id} name=${KMap[w][h].Button_id}`;
      text += ` type='button' class='kmapButtons' style='font-size:1em' value=' ${boolToBin(
        KMap[w][h].Value,
      )}`;
      // We give each box their K-Map entry data
      text += ` '; onClick=modifyKMEntry(KMap[${w}][${h}]);></td>`;
    }
    text += `</tr>`;
  }
  text += `</table>`;
  text += `</div>`;

  return text;
}

function getFunctionHTML(nCube, cubeId) {
  const ref = nCube.map(
    (space) =>
      toBinString(bitOrd[space[0]], KMap.nVarX) +
      toBinString(bitOrd[space[1]], KMap.nVarY),
  );

  const logicFunct = ref.reduce((result, space) => {
    return space.split("").map((bit, index) => {
      if (result[index] !== parseInt(bit)) {
        return 2;
      }
      return result[index];
    });
  }, ref[0].split("").map(Number));

  let funct = `<span id=${cubeId} onmouseover='setColor(nCubeList[${cubeId}], selectColor);' onmouseout='setColor(nCubeList[${cubeId}], normalColor);'>`;

  let wholeMap = true;
  logicFunct.forEach((value, k) => {
    if (value === 0) {
      wholeMap = false;
      funct += `<span style='text-decoration: overline'>${varNames[k]}</span>`;
    } else if (value === 1) {
      wholeMap = false;
      funct += varNames[k];
    }
  });

  // If whole map is covered, return 1
  funct += wholeMap ? "1" : "</span>";
  return funct;
}

function generateSolutionHTML() {
  setAllToNormalColor(); // Before anything, we set the K-Map's overal color to its normal, non-selected state.
  // We generate the solution.
  solve();
  // Class mono is used to make the text monospace
  let text = "<span class='mono'>";
  text += "<h2>";
	text += 'Q = '
  if (nCubeList.length === 0) {
    text += "0";
  } // Case where no spaces are covered.
  else {
		
    for (let i = 0; i < nCubeList.length; i++) {
      text += getFunctionHTML(nCubeList[i], i);
      if (i < nCubeList.length - 1) text += " + ";
    }
  }
  text += "</h2></span>";

  return text;
}

function redraw() {
  document.getElementById("KMapCalculator");
  document.getElementById("KMapDiv").innerHTML = generateKMapHTML();
  document.getElementById("SolutionDiv").innerHTML = generateSolutionHTML();
}

// Run initKMap with our default variable count
initKMap(numVar, 0);
